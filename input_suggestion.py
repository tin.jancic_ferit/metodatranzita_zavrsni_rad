from tkinter import *
import tkinter as tk
import pandas as pd
from tkinter.font import BOLD
from star_input import *

def ShowGraphSuggestion():
    inputValue = str(var.get())
    #flux.1 - flux.3197 => column names in csv file
    cols = []
    for i in range(1, 3198):
        cols.append('FLUX.' + str(i))

    #observations were made over aprox. 80 days
    time_series = pd.read_csv("time_sample_interval.csv")

    data = pd.read_csv("exoTrain.csv",usecols = cols,  skiprows=range(1, int(inputValue) - 1), nrows=1)
    fig = Figure(figsize=(6.5, 5), dpi=100)
    x_axis = time_series
    y_axis = data

    #plotting inside figure
    graph = fig.add_subplot(111)
    graph.plot(x_axis, y_axis, color = 'blue', linestyle = 'solid', marker='|')
    graph.set_xlabel("Time In Days")
    graph.set_ylabel("Relative Brightness") 
    graph.set_title("Light Curve")
    graph.grid()

    canv=FigureCanvasTkAgg(fig, master = root)  
    canv.draw()
    get_widz = canv.get_tk_widget()
    get_widz.grid(row=3, column=1, rowspan=50, columnspan=1)


suggestionLabel = Label(root, text = ' Good transit examples: ', font=('Arial', 9, BOLD))
suggestionLabel.grid(row=3, column=4, columnspan=5, sticky=N+W)

var = IntVar()
R1 = Radiobutton(root, text="Number 1", variable=var, value=1)
R1.grid(row=4, column=4, columnspan=2, sticky=N+W)
R2 = Radiobutton(root, text="Number 4", variable=var, value=4)
R2.grid(row=5, column=4, columnspan=2, sticky=N+W)
R3 = Radiobutton(root, text="Number 5", variable=var, value=5)
R3.grid(row=6, column=4, columnspan=2, sticky=N+W)
R4 = Radiobutton(root, text="Number 14", variable=var, value=14)
R4.grid(row=7, column=4, columnspan=2, sticky=N+W)
R5 = Radiobutton(root, text="Number 15", variable=var, value=15)
R5.grid(row=8, column=4, columnspan=2, sticky=N+W)
R6 = Radiobutton(root, text="Number 23", variable=var, value=23)
R6.grid(row=9, column=4, columnspan=2, sticky=N+W)
R7 = Radiobutton(root, text="Number 26", variable=var, value=26)
R7.grid(row=10, column=4, columnspan=2, sticky=N+W)

SuggestionButton = Button(root, text="Load Graph", bd=5, command=ShowGraphSuggestion)
SuggestionButton.grid(row=11, column=4, columnspan=2, sticky=N+W)
DefaultLabel = Label(root, text='Default: 1 ')
DefaultLabel.grid(row=12, column=4, columnspan=2, sticky=N+W)

