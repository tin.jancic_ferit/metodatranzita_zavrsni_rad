import tkinter as tk
from tkinter.font import BOLD
from GUI import *

def infoWindow():
    window = Toplevel()
    window.geometry('400x300')
    window.minsize(400, 300)
    window.maxsize(400, 300)
    window.title('Info')
    content = Text(window)
    content.tag_configure('title', font=('Verdana', 12, 'bold'), justify='center')
    content.tag_configure('paragraph', font=("Verdana", 10), justify='center')
    content.insert(END, "Transit Method\n", 'title')
    content.insert(END, 
    """                                                   
    Transit method is a photometric method that aims to 
    indirectly detect the presence of exoplanets in orbit    
    around a star. The transit is occured when an  
    exoplanet passes in front of its star. If luminosity 
    of a star is periodically decreased by approximately  
    the same value, we can say it is a good candidate for 
    future research and use of different methods to find  
    out if they really have an exoplanet/s in its orbit.   
    """                  
    , 'paragraph')

    content.configure(state='disabled')
    content.pack(padx=(5, 5), pady=(5, 5))

def helpWindow():
    window = Toplevel()
    window.geometry('500x350')
    window.minsize(500, 350)
    window.maxsize(500, 350)
    window.title('Help')
    content = Text(window)
    content.tag_configure('title', font=('Verdana', 12, 'bold'), justify='center')
    content.tag_configure('paragraph', font=("Verdana", 10), justify='center')
    content.insert(END, "How To Use\n", 'title')
    content.insert(END, 
    """                                                   
    This research consisted of observation of 5088 different stars over 
    the period of 80 days. After data analysis 38 stars were declared
    as candidates for having at least one exoplanet in its orbit.
    You can see the graphs of those stars if you type a number between
    1 and 38 in designated area (39 to 5088 otherwise). Also, the best 
    examples were given on the right side in the main window. To draw
    a graph, press Show Graph or Load Graph for already given examples.
    For candidate status press Status Button.
    If you want to know more about Transit Method press Info.
    """                  
    , 'paragraph')

    content.configure(state='disabled')
    content.pack(padx=(5, 5), pady=(5, 5))

mainmenu = Menu(Frame(root))
mainmenu.add_command(label="Info", command=infoWindow)
mainmenu.add_command(label="Help", command=helpWindow)
mainmenu.add_command(label="Exit", command=root.destroy)

root.config(menu = mainmenu)