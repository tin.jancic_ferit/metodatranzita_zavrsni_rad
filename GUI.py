from tkinter import *
import tkinter as tk
import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
matplotlib.use("TkAgg")

root = tk.Tk()
root.title('Exoplanet Hunting Using Transit Method')
root.iconbitmap('logo.ico')
root.geometry("850x600")
root.minsize(850, 600)
root.maxsize(850, 600)

#grid formatting 
empty_label1 = Label(root, text="        ")
empty_label1.grid(row=0, column=0)

empty_label2 = Label(root, text="        ")
empty_label2.grid(row=2, column=1)

empty_label3 = Label(root, text="  ")
empty_label3.grid(row=1, column=3)

empty_label4 = Label(root, text="    ")
empty_label4.grid(row=1, column=6)

empty_figure = Figure(figsize=(6.5, 5), dpi=100)
plot=empty_figure.add_subplot(111)

canvas = FigureCanvasTkAgg(empty_figure, master = root)  
canvas.draw()

get_widz = canvas.get_tk_widget()
get_widz.grid(row=3, column=1, rowspan=50, columnspan=1)




