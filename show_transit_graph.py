import tkinter as tk
import pandas as pd
import matplotlib
import tkinter.messagebox
from star_input import *
from input_suggestion import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
matplotlib.use("TkAgg")

def ShowGraph():
    inputValue = textbox.get("1.0", END)        
    #flux.1 - flux.3197 => column names in csv file
    cols = []
    for i in range(1, 3198):
        cols.append('FLUX.' + str(i))

    #observations were made over aprox. 80 days
    time_series = pd.read_csv("time_sample_interval.csv")

    data = pd.read_csv("exoTrain.csv",usecols = cols,  skiprows=range(1, int(inputValue) - 1), nrows=1)
    fig = Figure(figsize=(6.5, 5), dpi=100)
    x_axis = time_series
    y_axis = data

    #plotting inside figure
    graph = fig.add_subplot(111)
    graph.plot(x_axis, y_axis, color = 'blue', linestyle = 'solid', marker='|')
    graph.set_xlabel("Time In Days")
    graph.set_ylabel("Relative Brightness") 
    graph.set_title("Light Curve")
    graph.grid()

    canv=FigureCanvasTkAgg(fig, master = root)  
    canv.draw()
    get_widz = canv.get_tk_widget()
    get_widz.grid(row=3, column=1, rowspan=50, columnspan=1)


def StatusButtonClick():
    inputValue = textbox.get("1.0", END) 
    exoplanet_status = Label(root, text=" ")

    if int(inputValue) <= 38 and int(inputValue) > 0:
        exoplanet_status.config(text = "     This star is confirmed to have at least one exoplanet in orbit     ")
    elif int(inputValue) > 38 and int(inputValue) <= 5088:
        exoplanet_status.config(text = "This star is currently not considered to have any exoplanets in orbit")
    else:
        pass

    exoplanet_status.grid(row=2, column=1)


def Clicked():
    inputValue = textbox.get("1.0", END)
    true_input_value = inputValue.strip()
    
    if (true_input_value.isdigit() == False):
        tkinter.messagebox.showinfo("Error", "Please only input whole numbers")

        empty_figure = Figure(figsize=(6.5, 5), dpi=100)        
        plot=empty_figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(empty_figure, master = root)  
        canvas.draw()
        get_widz = canvas.get_tk_widget()
        get_widz.grid(row=3, column=1, rowspan=50, columnspan=1)

    elif int(true_input_value) > 5088 or int(true_input_value) <= 0:
        tkinter.messagebox.showinfo("Error", "Numbers need to be from the [1, 5088] interval")

        empty_figure = Figure(figsize=(6.5, 5), dpi=100)
        plot=empty_figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(empty_figure, master = root)  
        canvas.draw()
        get_widz = canvas.get_tk_widget()
        get_widz.grid(row=3, column=1, rowspan=50, columnspan=1)    
    else:
        pass
    
Show_graph_button = Button(root, text="Show graph", bd='5', command=lambda:[Clicked(), ShowGraph()])
Status_Button = Button(root, text="  Status  ",bd='5', command=lambda:[Clicked(), StatusButtonClick()])

Status_Button.grid(row=1, column=4)
Show_graph_button.grid(row=1, column=5)

